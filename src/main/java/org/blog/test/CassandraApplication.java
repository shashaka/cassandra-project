package org.blog.test;

import org.blog.test.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassandraApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(CassandraApplication.class);
    }

    @Autowired
    private UserService userService;

    @Override
    public void run(String... args) throws Exception {
        userService.addUser("testUser");
    }
}