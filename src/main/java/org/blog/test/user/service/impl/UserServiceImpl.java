package org.blog.test.user.service.impl;

import org.blog.test.user.entity.User;
import org.blog.test.user.repository.UserRepository;
import org.blog.test.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addUser(String name) {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setName(name);
        userRepository.save(user);
    }
}
